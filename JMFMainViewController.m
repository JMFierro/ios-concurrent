//
//  JMFMainViewController.m
//  Concurrent
//
//  Created by José Manuel Fierro Conchouso on 07/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import "JMFMainViewController.h"

@interface JMFMainViewController ()

@end

@implementation JMFMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Descarga Serie
- (IBAction)btnCargaSincronica:(id)sender {
    
    self.imgView.image = nil;
    
//    NSURL *url = [NSURL URLWithString:@"http://www.asturias.es/medioambiente/articulos/imagenes/ast%20(15).jpg"];
        NSURL *url = [NSURL URLWithString:@"http://2.bp.blogspot.com/-_bEiYzXhiFg/TrV1cZ57LqI/AAAAAAAAE-8/8-Djm0Hwmhg/s1600/paisaje4.jpg"];
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *img = [UIImage imageWithData:data];
    
    // No se puede ejecutar en segundo plano (UI)
    self.imgView.image = img;
    
}


//-(UIImage *)imgView2{
//    NSURL *url = [NSURL URLWithString:@"http://www.asturias.es/medioambiente/articulos/imagenes/ast%20(15).jpg"];
//    NSData *data = [NSData dataWithContentsOfURL:url];
//    UIImage *img = [UIImage imageWithData:data];
//
//    return img;
//}

-(void)imageWithCompletionBlock:(void (^)(UIImage *image))completionBlock {
//-(void)imageWithCompletionBlock:(void (^)(UIImage *image, NSString *url))completionBlock {

    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    
    dispatch_async(q, ^{
        
        
        NSURL *url = [NSURL URLWithString:@"http://2.bp.blogspot.com/-_bEiYzXhiFg/TrV1cZ57LqI/AAAAAAAAE-8/8-Djm0Hwmhg/s1600/paisaje4.jpg"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *img = [UIImage imageWithData:data];
        
        // EJECUCCIÓN DEL BLOQUE DE FINALIZACION EN LA COLA PRINCIPAL
        dispatch_async(dispatch_get_main_queue(), ^{
            // Asegurarse de que no es nil ! Es el único caso que en 'Object C' no se puede enviar 'nil'.
            if (completionBlock != nil) completionBlock(img);
        });
    });
}


-(void)imageDonloadWithTDidDonwladBlock: (void (^)(UIImage *img))didDownloadBlock {
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(q, ^{

        NSURL *url = [NSURL URLWithString:@"http://www.asturias.es/medioambiente/articulos/imagenes/ast%20(15).jpg"];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *img = [UIImage imageWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (didDownloadBlock != nil) didDownloadBlock(img);
        });
        
    });
    
    
    
    
}



- (IBAction)downloadParalelo:(id)sender {
    
     self.imgView.image = nil;
    

    
    [self imageDonloadWithTDidDonwladBlock:^(UIImage *img) {
        self.imgUp.image = img;
    }];
    
    [self imageWithCompletionBlock:^(UIImage *image) {
        self.imgView.image = image;
        
    }];
    
}



@end
