//
//  JMFMainViewController.h
//  Concurrent
//
//  Created by José Manuel Fierro Conchouso on 07/03/14.
//  Copyright (c) 2014 José Manuel Fierro Conchouso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMFMainViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgUp;

//@property (nonatomic, strong, readonly) UIImage *imgView2;

-(void)imageWithCompletionBlock:(void (^)(UIImage *))completionBlock;
//-(void)imageWithCompletionBlock:(void (^)(UIImage *,NSString *url))completionBlock;

-(void)imageDonloadWithTDidDonwladBlock: (void (^)(UIImage *img))didDownloadBlock;



- (IBAction)btnCargaSincronica:(id)sender;
- (IBAction)downloadParalelo:(id)sender;

@end
